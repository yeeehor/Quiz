import { Component, OnInit } from '@angular/core';
import {DataService} from '../service/data.service';
import {Observable} from 'rxjs/Observable';
import {QuizModel} from '../model/quiz.model';

@Component({
  selector: 'app-load-progress',
  templateUrl: './load-progress.component.html',
  styleUrls: ['./load-progress.component.css']
})
export class LoadProgressComponent implements OnInit {
  loading: boolean;
  constructor(private dataService: DataService) { }
  ngOnInit() {
    this.dataService.loading$.subscribe((isLoading) => {
      this.loading = isLoading;
    });
  }
}
