export class QuizModel {
  text: string;
  answers: string[];
  correctAnswer: number;
  checked: boolean;
  selected?: number;
}
