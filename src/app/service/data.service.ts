import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {QuizModel} from '../model/quiz.model';

@Injectable()
export class DataService {
  url = './assets/questionData.json';
  loading$ = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getQuestions(): Observable<QuizModel[]> {
      this.loading$.next(true);
      return this.http.get<QuizModel[]>(this.url).delay(500).do(() => {
        this.loading$.next(false);
      });
    }
}

